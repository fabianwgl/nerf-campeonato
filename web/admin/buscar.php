<?php 

require('../db.php');

   $sql1 = "SELECT * FROM usuarios";
$stmt1 = $conn->query($sql1);

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"/>
 
  <title>Nerf - Perfil</title>
<script src="https://use.typekit.net/zfw5zbz.js"></script>
<script>try{Typekit.load({ async: true });}catch(e){}</script>
<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
  <!-- CSS  -->
  
   <link href="//cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link href="../css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <link href="../css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <script type="text/javascript">
    $(document).ready(function(){
      $('.nerf').on('click',function(){
        var id = $(this).attr('id');
        console.log('ID '+id);
      });
      $('.ingress').on('click',function(){
        var id = $(this).attr('id');
        var newPuntaje = $('#puntaje'+id).val();
        var oldPuntaje = $('#oldPuntaje'+id).attr('data');        
        if(newPuntaje>=oldPuntaje){  
          console.log('mayor'); 
          $.post('../updatePuntaje.php',{'id':id,'puntaje':newPuntaje})
          .done(function(msg){            
            $('#ok').modal();
            location.reload();
          })
          .fail(function(xhr,status,error){
            $('#not').modal('open');
          });
          
        }else{
          //not
          $('#not').modal('open');
        }
      });
    });
  </script>

  <style>
      table.dataTable tbody tr {
       color: white !important;
    background-color: transparent !important;
}
#materialize-modal-overlay-1{
    display: none !important;
}
  </style>
</head>
<body>
  <nav role="navigation">
    <div class="nav-wrapper">
      <a href="#" class="brand-logo center"><img src="../images/logotour.png" alt="logotour" width="100%" height="" /></a>
       <ul id="navegacion" class="left hide-on-med-and-down">
        <li><a href="#">Home</a></li>
        <li><a href="#instrucciones">Cómo participar</a></li>
        <li><a href="#donde-participar">Dónde participar</a></li>
      </ul>
      <ul class="right hide-on-med-and-down" id="navegacion">
        <li><a href="#top5">Ranking</a></li>
        <li><a href="#premios">Premios</a></li>
        <li><a href="bases/Bases-de-Torneo-NERF%20TOURNAMENT-CHILE.docx">Bases legales</a></li>
        <li><a href="#" data-target="login" class="login">Login</a></li>
        <li><a href="http://www.nerf.com">Nerf.com</a></li>
      </ul>
      
      <ul id="nav-mobile" class="side-nav"> 
        <li><a href="#">Home</a></li>
        <li><a href="#">Cómo participar</a></li>
        <li><a href="#">Dónde participar</a></li>
        <li><a href="#">Ranking</a></li>
        <li><a href="#">Premios</a></li>
        <li><a href="#">Nerf.com</a></li>
        <li><a href="#">Bases legales</a></li>
        <li><a href="#">Login</a></li>
      </ul>
      
      <a href="#" data-activates="nav-mobile" class="button-collapse"><i class="material-icons">menu</i></a>
    </div>
	  
	
  </nav>

      <div id="login" class="modal">
	  <center>
			      <div class="row" style="margin-bottom: 10px">
			        <div class="input-field col s6 offset-s3">
			          <input id="email" type="email">
			          <label for="email">Email</label>
			        </div>
			      </div>
			       <div class="row" style="margin-bottom: 10px">
			        <div class="input-field col s6 offset-s3">
			          <input id="password" type="password">
			          <label for="password">Password</label>
			        </div>
			      </div>
	  </center>
	</div> 
	
	     
	
	    
	
	<div id="ok" class="modal">
	  <center>
			     Felicitaciones!!<br> has superado tu puntaje máximo anterior, <br>tu nuevo puntaje es: <br><b>199.999</b>
	  </center>
	</div> 
	
	
	 <div id="not" class="modal">
	  <center>
			     Que pena!!<br>  No has superado tu puntaje máximo anterior, <br>tu puntaje máximo sigue siendo: <br><b>199.999</b>
	  </center>
	</div> 

<div id="fondo" style="margin-top: 85px;">
  <div class="container">
    <div class="section" >

      <!--   Icon Section   -->
      <div class="row">
        <div class="col s1 m2 esconder">
        </div>

        <div class="col s12 m12 l8 metal" style="color:#fff" >
  

          	
	      
    <table id="myTable">
        <thead>
          <tr>
              <th>Nombre</th>
              <th>Mail</th>
              <th>Puntos</th>
               <th>Agregar</th>
          </tr>
        </thead>

        <tbody>
             <?php
              
                   while (  $row1 = $stmt1->fetch(PDO::FETCH_ASSOC)) {                    
              ?>
              <div id="modal<?php echo $row1['id']; ?>" class="modal" style="background: url(../images/fondo.jpg) center top; z-index:1009 !important; ">
	  <center>
			      <div class="row" style="margin-bottom: 10px">
				      <br><br><br><br>
			        <div class="input-field col s6 offset-s3">
                                    <input type="number" id="puntaje<?php echo $row1['id']; ?>" type="puntaje">
			          <label for="puntaje">Puntaje</label>
			        </div>
			      </div>
			      <br>
			       <div class="row" style="margin-top: 10px">
				<a href="#" id="<?php echo $row1['id']; ?>" class="waves-effect waves-light btn grey darken-4 ingress">Ingresar Puntaje</a>
			      </div>
			      <br><br><br><br>
	  </center>
	</div> 
          <tr>
            <td><?php echo $row1['nombre'];?> <?php echo $row1['apellido'];?></td>
            <td><?php echo $row1['email'];?></td>
            <td id="oldPuntaje<?php echo $row1['id']; ?>" data="<?php echo $row1['puntaje'];?>"> <?php echo $row1['puntaje'];?></td>
              <td><a href="#" data-target="modal<?php echo $row1['id']; ?>" id="<?php echo $row1['id']; ?>" class="waves-effect waves-light btn grey darken-4 nerf">Agregar Puntaje</a></td>
          </tr>
           <?php
              
                   }
              ?>
        </tbody>
      </table>

    

        <div class="col s2 m2">
        </div>
        
      </div>

    </div>
  </div>
</div>


  <footer class="page-footer teal" style="z-index: 9;">
    <div class="container">
      <div class="row">
        <div class="col l3 s6">
          <center><img src="../images/logonerf.png" alt="logonerf" width="90%" style="padding: 40px 0;"></center>


        </div>
        <div class="col l2 s6">
          <ul class="menufooter">
        <li><a href="#">Home</a></li>
        <li><a href="#">Dónde participar</a></li>
        <li><a href="#">Cómo participar</a></li>
        <li><a href="#">Ranking</a></li>
        <li><a href="#">Premios</a></li>
        <li><a href="#">Nerf.com</a></li>
        <li><a href="#">Bases legales</a></li>
        <li><a href="#">Login</a></li>
          </ul>
        </div>
        <div class="col l7 s12">
          <img src="../images/footer.png" alt="footer" width="100%" height="" />
        </div>
      </div>
    </div>
    <div class="footer-copyright">
      <div class="container">
      Desarrollado por <a class="brown-text text-lighten-3" href="http://chucara.cl">Chúcara</a>
      </div>
    </div>
  </footer>


  <!--  Scripts-->
  
  <script src="../js/materialize.js"></script>
  <script src="../js/init.js"></script>
    <script src="//cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
 
  <script>
    $( document ).ready(function() {
      $('.modal').modal();
      $('#modal1').on('click', function() {
      });
      $('.nerf').click(function(){
           var id = $(this).attr('data-target');
           console.log(id);
      });
      
    });
    
    $(document).ready(function(){
      $('#myTable').DataTable();
    });
  </script>

  <script>
      $(function(){
           
      });
  </script>

  </body>
</html>
