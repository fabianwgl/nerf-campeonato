 
<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"/>
  <title>Nerf - Perfil</title>
<script src="https://use.typekit.net/zfw5zbz.js"></script>
<script>try{Typekit.load({ async: true });}catch(e){}</script>

  <!-- CSS  -->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link href="../css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <link href="../css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>
</head>
<body>
  <nav role="navigation">
    <div class="nav-wrapper">
      <a href="#" class="brand-logo center"><img src="../images/logotour.png" alt="logotour" width="100%" height="" /></a>
       <ul id="navegacion" class="left hide-on-med-and-down">
        <li><a href="#">Home</a></li>
        <li><a href="#instrucciones">Cómo participar</a></li>
        <li><a href="#donde-participar">Dónde participar</a></li>
      </ul>
      <ul class="right hide-on-med-and-down" id="navegacion">
        <li><a href="#top5">Ranking</a></li>
        <li><a href="#premios">Premios</a></li>
        <li><a href="bases/Bases-de-Torneo-NERF%20TOURNAMENT-CHILE.docx">Bases legales</a></li>
        <li><a href="#" class="login">Login</a></li>
        <li><a href="http://www.nerf.com">Nerf.com</a></li>
      </ul>
      
      <ul id="nav-mobile" class="side-nav"> 
        <li><a href="#">Home</a></li>
        <li><a href="#">Cómo participar</a></li>
        <li><a href="#">Dónde participar</a></li>
        <li><a href="#">Ranking</a></li>
        <li><a href="#">Premios</a></li>
        <li><a href="#">Nerf.com</a></li>
        <li><a href="#">Bases legales</a></li>
        <li><a href="#">Login</a></li>
      </ul>
      
      <a href="#" data-activates="nav-mobile" class="button-collapse"><i class="material-icons">menu</i></a>
    </div>
	  
	
  </nav>


<div id="fondo" style="margin-top: 85px;">
  <div class="container">
    <div class="section" >

      <!--   Icon Section   -->
      <div class="row">
        <div class="col s1 m2 esconder">
        </div>

        <div class="col s12 m12 l8 metal" >
            <form action="login.php" method="post">
                
             
						      <div class="row" style="margin-bottom: 10px;padding-top: 40px;">
						        <div class="input-field col s6 offset-s3">
                                                            <input id="email" type="email" name="mail">
						          <label for="email">Email</label>
						        </div>
						      </div>
						       <div class="row" style="margin-bottom: 10px;padding-bottom: 70px;">
						        <div class="input-field col s6 offset-s3">
                                                            <input id="password" type="password" name="pw">
						          <label for="password">Password</label>
						        </div>
						      </div>
                <input type="submit" hidden="">
            </form>
				</div> 
        </div>

        <div class="col s2 m2">
        </div>
        
      </div>

    </div>
  </div>
</div>


  <footer class="page-footer teal" style="z-index: 9;">
    <div class="container">
      <div class="row">
        <div class="col l3 s6">
          <center><img src="../images/logonerf.png" alt="logonerf" width="90%" style="padding: 40px 0;"></center>


        </div>
        <div class="col l2 s6">
          <ul class="menufooter">
        <li><a href="#">Home</a></li>
        <li><a href="#">Dónde participar</a></li>
        <li><a href="#">Cómo participar</a></li>
        <li><a href="#">Ranking</a></li>
        <li><a href="#">Premios</a></li>
        <li><a href="#">Nerf.com</a></li>
        <li><a href="#">Bases legales</a></li>
        <li><a href="#">Login</a></li>
          </ul>
        </div>
        <div class="col l7 s12">
          <img src="../images/footer.png" alt="footer" width="100%" height="" />
        </div>
      </div>
    </div>
    <div class="footer-copyright">
      <div class="container">
      Desarrollado por <a class="brown-text text-lighten-3" href="http://chucara.cl">Chúcara</a>
      </div>
    </div>
  </footer>


  <!--  Scripts-->
  <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
  <script src="../js/materialize.js"></script>
  <script src="../js/init.js"></script>

  </body>
</html>
