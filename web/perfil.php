<?php
require 'db.php';
session_start();

$id = $_SESSION['id'];

$sql = "SELECT * FROM usuarios WHERE id='$id'";
$stmt = $conn->query($sql);
$row = $stmt->fetch(PDO::FETCH_ASSOC);
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"/>
        <title>Nerf - Perfil</title>
        <script src="https://use.typekit.net/zfw5zbz.js"></script>
        <script>try {
                Typekit.load({async: true});
            } catch (e) {
            }</script>

        <!-- CSS  -->
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link href="css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>
        <link href="css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>
    </head>
    <body>
        <nav role="navigation">
            <div class="nav-wrapper">
                <a href="#" class="brand-logo center"><img src="images/logotour.png" alt="logotour" width="100%" height="" /></a>
                <ul id="navegacion" class="left hide-on-med-and-down">
                    <li><a href="#">Home</a></li>
                    <li><a href="#instrucciones">Cómo participar</a></li>
                    <li><a href="#donde-participar">Dónde participar</a></li>
                </ul>
                <ul class="right hide-on-med-and-down" id="navegacion">
                    <li><a href="#top5">Ranking</a></li>
                    <li><a href="#premios">Premios</a></li>
                    <li><a href="bases/Bases-de-Torneo-NERF%20TOURNAMENT-CHILE.docx">Bases legales</a></li>
                    <li><a href="#" class="login">Login</a></li>
                    <li><a href="http://www.nerf.com">Nerf.com</a></li>
                </ul>

                <ul id="nav-mobile" class="side-nav"> 
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Cómo participar</a></li>
                    <li><a href="#">Dónde participar</a></li>
                    <li><a href="#">Ranking</a></li>
                    <li><a href="#">Premios</a></li>
                    <li><a href="#">Nerf.com</a></li>
                    <li><a href="#">Bases legales</a></li>
                    <li><a href="#">Login</a></li>
                </ul>

                <a href="#" data-activates="nav-mobile" class="button-collapse"><i class="material-icons">menu</i></a>
            </div>


        </nav>


        <div id="fondo" style="margin-top: 85px;">
            <div class="container">
                <div class="section" >

                    <!--   Icon Section   -->
                    <div class="row">
                        <div class="col s1 m2 esconder">
                        </div>

                        <div class="col s12 m12 l8 metal" >
                            <div class="row">    
                                <div class="col s12 m6 offset-m3 l4">
                                    <div class="titulo">PERFIL</div>
                                    <div class="icon-block">
                                        <form action="newavatar.php" id="myform" method="post" enctype="multipart/form-data">
                                        <div id="foto" style="background:url(images/<?php echo $row['foto'] ?>);"><img  src="images/marcofoto.png" alt="marcofoto" width="100%" height="" /></div>
                                        <input type='file' id="imgInp" hidden=""  name="imagen"/>
 
                                        <div id="chile1"></div>
                                        </form>
                                    </div>
                                </div>  
                                <div class="col s12 m10 offset-m1 l8">
                                    <div class="datos">
                                        <div class="row" style="margin: 0;padding: 5px;">    
                                            <div class="col s6 m6">
                                                <center>Puntaje Máximo<br><span><?php echo $row['puntaje'] ?></span></center>
                                            </div>   
                                            <div class="col s6 m6">
                                                <center>Ranking<br><span><?php
                                                        $sql2 = "SELECT * FROM usuarios  ORDER BY puntaje DESC";
                                                        $stmt3 = $conn->query($sql2);

                                                        $cont = 0;
                                                        while ($row2 = $stmt3->fetch(PDO::FETCH_ASSOC)) {
                                                            $cont++;
                                                            if ($row2['id'] == $id) {
                                                                echo $cont;
                                                                 break; 
                                                            }
                                                            
                                                        }
                                                        ?>º Lugar </span></center>
                                            </div>    
                                        </div> 	
                                    </div>
                                    <div class="titulo1"><?php echo $row['nombre'] ?> <?php echo $row['apellido'] ?></div>
                                    <div class="info">
                                        <b>Edad: </b><?php echo $row['edad'] ?> años<br>
                                        <b>Mail: </b><?php echo $row['email'] ?><br>
                                        <b>Teléfono: </b><?php echo $row['telefono'] ?></div>     


                                    <br>

                                    <img src="images/mensajes.png" alt="mensajes" width="40%" height="" style="z-index: 2;
                                         position: relative;" />

                                    <select class="browser-default test">
                                        <option value="" disabled selected class="prueba">selecciona tu mensaje</option>
                                        <option value="" disabled>Desafío</option>    
                                        <option value="¿Quién será el siguiente que perderá conmigo?">¿Quién será el siguiente que perderá conmigo?</option>
                                        <option value="Menos blá blá y más lanzamientos ¿quién se anota?">Menos blá blá y más lanzamientos ¿quién se anota?</option>
                                        <option value="¿Quién será el siguiente que perderá conmigo?">¿Quién será el siguiente que perderá conmigo?</option>
                                        <option value="Los espero en el torneo aprendices.">Gánenme si pueden ¡Este torneo ya tiene dueño! </option>
                                        <option value="¿Quién será el siguiente que perderá conmigo?">¿Quién será el siguiente que perderá conmigo?</option>
                                        <option value="Pasen a hablar con el próximo campeón Nerf de Latinoamérica">¡Devuélvanse a sus casas! Este torneo está a mi nivel PRO</option>
                                        <option value="" disabled>Autoreferente</option>    
                                        <option value="Les presento al mejor Nerf Gamer de América: YO">Les presento al mejor Nerf Gamer de América: YO</option>
                                        <option value="No me verán ni la sombra ¡comiencen a practicar!">No me verán ni la sombra ¡comiencen a practicar!</option>
                                        <option value="Todos mis dardos van directo al objetivo ¿y los tuyos?">Todos mis dardos van directo al objetivo ¿y los tuyos?</option>
                                        <option value="Este es mi momento de vencer, nadie me quitará la gloria ">Este es mi momento de vencer, nadie me quitará la gloria </option>
                                        <option value="No conozco la palabra derrota ¿ustedes si verdad?">No conozco la palabra derrota ¿ustedes si verdad?</option>
                                        <option value="Cuando compito lo hago para ganar ¡así es como juego!">Cuando compito lo hago para ganar ¡así es como juego!</option>
                                        <option value="Conmigo aquí el 1er lugar del ranking está lleno ¡Deal with it!">Conmigo aquí el 1er lugar del ranking está lleno ¡Deal with it!</option>
                                        <option value="" disabled>Consejos</option>    
                                        <option value="¡Mejor practiquen sus lanzamientos! Conmigo la competencia esta difícil">¡Mejor practiquen sus lanzamientos! Conmigo la competencia esta difícil</option>
                                        <option value="¿Lanzador? ¡check! ¿dardos? ¡check! ¿GAMER ON FIRE? ¡double check!">¿Lanzador? ¡check! ¿dardos? ¡check! ¿GAMER ON FIRE? ¡double check!</option>
                                        <option value="Todos mis dardos van directo al objetivo ¿y los tuyos?">Todos mis dardos van directo al objetivo ¿y los tuyos?</option>
                                        <option value="Siempre en movimiento y con la mira la victoria ¡así es como jugamos!">Siempre en movimiento y con la mira la victoria ¡así es como jugamos!</option>
                                    </select>


                                    <div id="mensajes">
                                        <div id="fila" class="row">
                                            <div class="col s4 m3">
                                                <div id="foto" style="background:url(images/mati.jpg);"><img src="images/marcofoto.png" alt="marcofoto" width="100%" height="" /></div>
                                                <div id="lugarr">1er Lugar</div>
                                            </div>
                                            <div class="col s8 m9">
                                                Mensaje enviado y generado
                                            </div>
                                        </div>

                                        <div id="fila" class="row">

                                            <div class="col s8 m9">
                                                Mensaje enviado y generado
                                            </div>
                                            <div class="col s4 m3">
                                                <div id="foto" style="background:url(images/mati.jpg);"><img src="images/marcofoto.png" alt="marcofoto" width="100%" height="" /></div>
                                                <div id="lugarr">1er Lugar</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="linea row">
                                <div class="boton" style="cursor: pointer" id="enviaravatar">
                                    Guardar Avatar
                                </div>
                                <div class="boton1">
                                    Enviar Mensaje
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col s2 m2">
                    </div>

                </div>

            </div>
        </div>
    </div>


    <footer class="page-footer teal" style="z-index: 9;">
        <div class="container">
            <div class="row">
                <div class="col l3 s6">
                    <center><img src="images/logonerf.png" alt="logonerf" width="90%" style="padding: 40px 0;"></center>


                </div>
                <div class="col l2 s6">
                    <ul class="menufooter">
                        <li><a href="#">Home</a></li>
                        <li><a href="#">Dónde participar</a></li>
                        <li><a href="#">Cómo participar</a></li>
                        <li><a href="#">Ranking</a></li>
                        <li><a href="#">Premios</a></li>
                        <li><a href="#">Nerf.com</a></li>
                        <li><a href="#">Bases legales</a></li>
                        <li><a href="#">Login</a></li>
                    </ul>
                </div>
                <div class="col l7 s12">
                    <img src="images/footer.png" alt="footer" width="100%" height="" />
                </div>
            </div>
        </div>
        <div class="footer-copyright">
            <div class="container">
                Desarrollado por <a class="brown-text text-lighten-3" href="http://chucara.cl">Chúcara</a>
            </div>
        </div>
    </footer>


    <!--  Scripts-->
    <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
    <script src="../js/materialize.js"></script>
    <script src="../js/init.js"></script>
<script>
$("#foto").click(function() {
    $("#imgInp").click();
});
$("#enviaravatar").click(function() {
    $("#myform").submit();
});
function readURL(input) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) { 
            $('#foto').css('background-image', 'url(' +e.target.result+ ')');
        } 
        reader.readAsDataURL(input.files[0]);
    }
}

$("#imgInp").change(function(){
    readURL(this);
});
</script>
</body>
</html>
