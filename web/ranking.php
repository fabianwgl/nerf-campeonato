<?php 

require('db.php');

   $sql1 = "SELECT * FROM usuarios ORDER BY `usuarios`.`puntaje` DESC";
$stmt1 = $conn->query($sql1);

?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"/>
  <title>Nerf - Ranking </title>

<script src="https://use.typekit.net/zfw5zbz.js"></script>
<script>try{Typekit.load({ async: true });}catch(e){}</script>

  <!-- CSS  -->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link href="css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <link href="css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>
 
</head>
<body>
  <nav role="navigation">
    <div class="nav-wrapper">
      <a href="#" class="brand-logo center"><img src="images/logotour.png" alt="logotour" width="100%" height="" /></a>
       <ul id="navegacion" class="left hide-on-med-and-down">
        <li><a href="#">Home</a></li>
        <li><a href="#instrucciones">Cómo participar</a></li>
        <li><a href="#donde-participar">Dónde participar</a></li>
      </ul>
      <ul class="right hide-on-med-and-down" id="navegacion">
        <li><a href="#top5">Ranking</a></li>
        <li><a href="#premios">Premios</a></li>
        <li><a href="bases/Bases-de-Torneo-NERF%20TOURNAMENT-CHILE.docx">Bases legales</a></li>
        <li><a href="#" class="login">Login</a></li>
        <li><a href="http://www.nerf.com">Nerf.com</a></li>
      </ul>
      
      <ul id="nav-mobile" class="side-nav"> 
        <li><a href="#">Home</a></li>
        <li><a href="#">Cómo participar</a></li>
        <li><a href="#">Dónde participar</a></li>
        <li><a href="#">Ranking</a></li>
        <li><a href="#">Premios</a></li>
        <li><a href="#">Nerf.com</a></li>
        <li><a href="#">Bases legales</a></li>
        <li><a href="#">Login</a></li>
      </ul>
      
      <a href="#" data-activates="nav-mobile" class="button-collapse"><i class="material-icons">menu</i></a>
    </div>
	  
	
  </nav>





<div id="mundial">
  <div class="">
    <div class="section"> 
      <div class="row">
	      <br><br><br>
	      <center><input type="text" name="search" placeholder="Buscar" style="width: 25%;
    float: right;
    margin: 10px;"></center>


	      <br><br><br>
	      <br><br><br>
              <?php
              $cont=0;
                   while (  $row1 = $stmt1->fetch(PDO::FETCH_ASSOC)) {
                     $cont++;

              ?>
        <div class="col s6 m4 l2">
          <div class="icon-block">
          	<div id="foto" style="background:url(images/<?php echo $row1['foto'];?>);"><img src="images/marcofoto.png" alt="marcofoto" width="100%" height="" /></div>
          	<div id="lugar"><?php echo $cont;?>° Lugar</div>
            <div id="puntaje"><?php echo $row1['puntaje'];?></div>
          	<div id="chile"></div>
          </div>
        </div>
               <?php
              
                   }
              ?>

      
      </div>
      
      

<div style="text-align: right; margin-right: 30px; font-size: 40px; color: #fff; font-family: 'orbitron',sans-serif; text-shadow:1px 1px 10px #08d9ec"><i>Siguiente</i></div>

    </div>
  </div>
</div>

	      
	      
	      
	      
	      

  <footer class="page-footer teal">
    <div class="container">
      <div class="row">
        <div class="col l3 s6">
          <center><img src="images/logonerf.png" alt="logonerf" width="90%" style="padding: 40px 0;"></center>


        </div>
        <div class="col l2 s6">
          <ul class="menufooter">
        <li><a href="#">Home</a></li>
        <li><a href="#">Dónde participar</a></li>
        <li><a href="#">Cómo participar</a></li>
        <li><a href="#">Ranking</a></li>
        <li><a href="#">Premios</a></li>
        <li><a href="#">Nerf.com</a></li>
        <li><a href="#">Bases legales</a></li>
        <li><a href="#">Login</a></li>
          </ul>
        </div>
        <div class="col l7 s12">
          <img src="images/footer.png" alt="footer" width="100%" height="" />
        </div>
      </div>
    </div>
    <div class="footer-copyright">
      <div class="container">
      Desarrollado por <a class="brown-text text-lighten-3" href="http://chucara.cl">Chúcara</a>
      </div>
    </div>
  </footer>


  <!--  Scripts-->
  <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
  <script src="js/materialize.js"></script>
  <script src="js/init.js"></script>
  <script>
	  <script>
 $(document).ready(function(){
    // the "href" attribute of .modal-trigger must specify the modal ID that wants to be triggered
    $('.modal').modal();
  });
</script>
	  </script>

  </body>
</html>
