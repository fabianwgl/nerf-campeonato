<?php 
class Principal_model extends CI_Model {

    public function insertarRegistro($nombre,$apellido,$telefono,$pais,$email,$password,$edad,$puntaje){
        $data = array(
                'nombre' => $nombre,
                'apellido' => $apellido,
                'telefono' => $telefono,
                'pais' => $pais,
                'email' => $email,
                'password' => $password,
                'edad' => $edad,
                'puntaje' => $puntaje,
                'intento' => 1
            );
        $query = $this->db->insert('usuarios', $data);
        if($query):
            return true;
        else:
            return false;
        endif;
    }
    
    public function posicion($id){
	    $query = $this->db->query("SELECT id, nombre, puntaje, FIND_IN_SET( puntaje, ( SELECT GROUP_CONCAT( puntaje ORDER BY puntaje DESC ) FROM usuarios ) ) AS rank FROM usuarios WHERE id = '$id'
");
		return $query->result_array();
    }
    
    
    public function buscarJugador($buscar){
		  $query = $this->db->query("SELECT * FROM usuarios WHERE nombre LIKE '%".$buscar."%' OR apellido LIKE '%".$buscar."%'");
		  return $query->result();
    }

    public function busqueda($nombre){
        $query = $this->db->query("SELECT * FROM usuarios WHERE nombre LIKE '%".$nombre."%' OR apellido LIKE '%".$nombre."%'");
        if($query->num_rows() > 0):
            return $query->result_array();
        else:
            return false;
        endif;
    }
    
    public function imagen($userfiles, $id){
	    $this->db->query("UPDATE usuarios SET foto = '$userfiles' WHERE id = '$id'");
	    return true;
    }

    public function sumarPuntaje($id,$puntaje,$intento,$actual){
        $intento = $intento + 1;
        $puntaje_actual = $puntaje + $actual;
        if($intento < 4):
            $array = array('puntaje' => $puntaje_actual,
                            'intento' => $intento,
                            );
            $this->db->where('id', $id);
            $query = $this->db->update('usuarios', $array);
            return true;
        else:
            return false;
        endif;
    }

    public function mostrarMensajesInicio(){
        $query = $this->db->query("SELECT * FROM mensajes LIMIT 5");
        if($query->num_rows() > 0):
            return $query->result_array();
        else:
            return false;
        endif;
    }

    public function mostrarMensajesUsuario($id){
        $query = $this->db->query("SELECT * FROM usuarios, mensajes WHERE usuarios.id = mensajes.usuario_id");
        if($query->num_rows() > 0):
            return $query->result_array();
        else:
            return false;
        endif;
    }

    public function rankingTotal(){
        $query = $this->db->query("SELECT * FROM usuarios ORDER BY puntaje DESC LIMIT 25 ");
        if($query->num_rows() > 0):
            return $query->result_array();
        else:
            return false;
        endif;
    }

    public function rankingPiola(){
        $query = $this->db->query("SELECT * FROM usuarios ORDER BY puntaje DESC LIMIT 5 ");
        if($query->num_rows() > 0):
            return $query->result_array();
        else:
            return false;
        endif;
    }
    
    public function inicioSesion($usuario, $password){
	    $query = $this->db->query("SELECT * FROM usuarios WHERE email = '$usuario' and password = '$password'");
	    if($query->num_rows() > 0):
	    	return $query->result_array();
	    else:
	    	return false;
	    endif;
    }
    
    public function insertarMensaje($id,$mensaje){
	    $data = array(
		    	'usuario_id' => $id,
                'mensaje' => $mensaje
            );
        $query = $this->db->insert('mensajes', $data);
        if($query):
            return true;
        else:
            return false;
        endif;
    }

}