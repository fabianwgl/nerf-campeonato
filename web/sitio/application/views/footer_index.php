<footer class="page-footer teal">
    <div class="container">
      <div class="row">
        <div class="col l3 s6">
          <center><img src="../images/logonerf.png" alt="logonerf" width="90%" style="padding: 40px 0;"></center>


        </div>
        <div class="col l2 s6">
          <ul class="menufooter">
        <li><a href="#">Home</a></li>
        <li><a href="#">Dónde participar</a></li>
        <li><a href="#">Cómo participar</a></li>
        <li><a href="#">Ranking</a></li>
        <li><a href="#">Premios</a></li>
        <li><a href="#">Nerf.com</a></li>
        <li><a href="#">Bases legales</a></li>
        <li><a href="#">Login</a></li>
          </ul>
        </div>
        <div class="col l7 s12">
          <img src="../images/footer.png" alt="footer" width="100%" height="" />
        </div>
      </div>
    </div>
    <div class="footer-copyright">
      <div class="container">
      Desarrollado por <a class="brown-text text-lighten-3" href="http://chucara.cl">Chúcara</a>
      </div>
    </div>
  </footer>


  <!--  Scripts-->
  <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
  <script src="js/materialize.js"></script>
  <script src="js/init.js"></script>
<script>
    $( document ).ready(function() {
      $('.modal').modal();
      $('#modal1').on('click', function() {
      });
    });
  </script>
	  </script>
	  
<script>
 $(document).ready(function(){
   $("#search").keyup(function(){
  if($("#search").val().length>3){
  $.ajax({
   type: "post",
   url: "http://www.nerflatam2017.com/web/sitio/welcome/buscarUsuario",
   cache: false,    
   data:'search='+$("#search").val(),
   success: function(response){
    $('#finalResult').html("");
    var obj = JSON.parse(response);
    if(obj.length>0){
     try{
      var items=[];  
      $.each(obj, function(i,val){           
          items.push($('<li/>').text(val.nombre + " " + val.apellido));
      }); 
      $('#finalResult').append.apply($('#finalResult'), items);
     }catch(e) {  
      alert('Exception while request..');
     }  
    }else{
     $('#finalResult').html($('<li/>').text("No hay coincidencias"));  
    }  
    
   },
   error: function(){      
    alert('Error');
   }
  });
  }
  return false;
   });
 });
</script>

  </body>
</html>
