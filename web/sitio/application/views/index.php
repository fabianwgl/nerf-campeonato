<body>
  <nav role="navigation">
    <div class="nav-wrapper">
      <a href="#" class="brand-logo center"><img src="images/logotour.png" alt="logotour" width="100%" height="" /></a>
       <ul id="navegacion" class="left hide-on-med-and-down">
        <li><a href="#">Home</a></li>
        <li><a href="#instrucciones">Cómo participar</a></li>
        <li><a href="#donde-participar">Dónde participar</a></li>
      </ul>
      <ul class="right hide-on-med-and-down" id="navegacion">
        <li><a href="#top5">Ranking</a></li>
        <li><a href="#premios">Premios</a></li>
        <li><a href="../bases/Bases-de-Torneo-NERF%20TOURNAMENT-CHILE.docx">Bases legales</a></li>
        <li><a href="#" data-target="login" class="login">Login</a></li>
        <li><a href="http://www.nerf.com">Nerf.com</a></li>
      </ul>
      

      
      <ul id="nav-mobile" class="side-nav"> 
        <li><a href="#">Home</a></li>
        <li><a href="#">Cómo participar</a></li>
        <li><a href="#">Dónde participar</a></li>
        <li><a href="#">Ranking</a></li>
        <li><a href="#">Premios</a></li>
        <li><a href="#">Nerf.com</a></li>
        <li><a href="#">Bases legales</a></li>
        <li><a href="#" data-target="login">Login</a></li>
      </ul>
      
      <a href="#" data-activates="nav-mobile" class="button-collapse"><i class="material-icons">menu</i></a>
    </div>
	  
	
  </nav>
  
      <div id="login" class="modal">
	  <center>
	  		<form method="post" action="welcome/login">
			      <div class="row" style="margin-bottom: 10px">
			        <div class="input-field col s6 offset-s3">
			          <input id="email" name="email" type="email">
			          <label for="email">Email</label>
			        </div>
			      </div>
			       <div class="row" style="margin-bottom: 10px">
			        <div class="input-field col s6 offset-s3">
			          <input id="password" name="password" type="password">
			          <label for="password">Password</label>
			        </div>
			      </div>
			      <br>
			      <input type="submit" />
			</form>	  
	  </center>
	</div> 

  <div id="index-banner" class="parallax-container">
    <div class="section no-pad-bot">
    <div style="position: fixed; z-index: -99; width: 100%; height: 100%; margin-top: 100px">
  <iframe frameborder="0" height="100%" width="100%" 
    src="https://youtube.com/embed/kKOmdQ5naYw?loop=1&autoplay=1&controls=0&showinfo=0&autohide=1&rel=0">
  </iframe>
</div>
    </div>
  </div>

	      <img src="images/nerf.png" alt="nerf" width="100%" height="" style="margin-top: -33px;" />
<div id="instrucciones">
  <div class="container">
    <div class="section">
      <!--   Icon Section   -->
      <div class="row">
        <div class="col s12 m12 l3">
          <div class="icon-block">
	        <div class="caja">
				<img src="images/paso1.png" alt="paso1" width="100%" height="" />  
	        </div>
            <h2 class="center white-text">1</h2>
            <h5>Regístrate en los módulos Nerf en los puntos de venta</h5>

          </div>
        </div>

        <div class="col s12 m12 l3">
          <div class="icon-block">
	        <div class="caja">
				<img src="images/paso2.png" alt="paso1" width="100%" height="" />  
	        </div>
            <h2 class="center white-text">2</h2>
            <h5>Juega en los módulos Nerf Tournament en tiendas</h5>
          </div>
        </div>

        <div class="col s12 m12 l3">
          <div class="icon-block">
	        <div class="caja">
				<img src="images/paso3.png" alt="paso1" width="100%" height="" />  
	        </div> 
            <h2 class="center white-text">3</h2>
            <h5>Acumula el máximo puntaje jugando en los diferentes escenarios</h5>
          </div>
        </div>
        
        <div class="col s12 12 l3">
          <div class="icon-block">
	        <div class="caja">
				<img src="images/paso4.png" alt="paso1" width="100%" height="" />  
	        </div>  
            <h2 class="center white-text">4</h2>
            <h5>Conviértete en el mejor Nerf Gamer de Latinoamérica</h5>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>

  <div id="donde-participar">
    <div class="container">
		<div class="row mapita">
			<div class="col s12 m12 l6">
				<img src="images/chilemapa.png" width="100%" alt="mapa">
			</div>
			<div class="col s12 m6 l3">
				<div class="punto">
					<img src="images/13-14-mayo.png" alt="13-14-mayo" width="100%" height="" />    
					<div class="morado">1</div><div class="detalle" data-target="modal1">· Evento Fanatic en Concepción: Talcahuano Nº8590, Hualpén.</div><br><br>
					<div class="rojo">2</div><div class="detalle" data-target="modal2">· Líder: Américo Vespucio 1737, Huechuraba.</div>
				</div>
				<div class="punto">
					<img src="images/20-21-mayo.png" alt="20-21-mayo" width="100%" height="" />
					<div class="rojo">3</div><div class="detalle" data-target="modal3">· Lider Gran Avenida: Gran Avenida 6150, San Miguel.</div><br><br>
					<div class="rojo">4</div><div class="detalle" data-target="modal4">· Tottus Kennedy: Avenida Kennedy 5601, Las Condes.</div>
				</div>
				<div class="punto">
					<img src="images/27-28-mayo.png" alt="27-28-mayo" width="100%" height="" />
					<div class="rojo">5</div><div class="detalle" data-target="modal5">· Lider La Florida: Av. Santa Amalia 1763, La Florida.</div><br><br>
					<div class="rojo">6</div><div class="detalle" data-target="modal6">· Tottus Buin: San Martín 173, Buin.</div>
				</div>
			</div>
			<div class="col s12 m6 l3">
				<div class="punto">
					<img src="images/03-04-junio.png" alt="03-04-junio" width="100%" height="" />
					<div class="rojo">7</div><div class="detalle" data-target="modal7">· Lider Puente Alto: Av. Concha y Toro 1149, Puente Alto.</div><br><br>
					<div class="rojo">8</div><div class="detalle" data-target="modal8">· Tottus Nataniel: Nataniel Cox 620, Santiago Centro.</div>
				</div>
				<div class="punto">
					<img src="images/10-11-junio.png" alt="10-11-junio" width="100%" height="" />
					<div class="rojo">9</div><div class="detalle" data-target="modal9">· Lider Maipu: Av. Pajaritos 4500, Maipu.</div><br><br>
					<div class="rojo1">10</div><div class="detalle" data-target="modal10">· Paris: Mall Plaza Oeste, Cerrillos.</div>
				</div>
				<div class="punto">
					<img src="images/17-18-junio.png" alt="17-18-junio" width="100%" height="" />
					<div class="rojo1">11</div><div class="detalle" data-target="modal11">· Lider Alameda: Av. Padre Alberto Hurtado 060, Estación Central.</div><br><br>
					<div class="rojo1">12</div><div class="detalle" data-target="modal12">· Paris: Mall Parque Arauco, Las Condes.</div>
				</div>
			</div>
		</div>
    </div>
  </div>
  
<!-- Modal Structure -->
<div id="modal1" class="modal">
	  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3195.0987373554726!2d-73.07405098471034!3d-36.79218297995022!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9669b576c18f542f%3A0xb76a70af922d0fa3!2sSurActivo!5e0!3m2!1ses!2scl!4v1494280034386" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>   
<!-- Modal Structure -->
<div id="modal2" class="modal">
	  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d53305.01901393943!2d-70.66793227669383!3d-33.38245518751421!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9662c6fd067b8cf3%3A0xdffb4986cf4249fc!2sAv.+Am%C3%A9rico+Vespucio+1737%2C+Huechuraba%2C+Regi%C3%B3n+Metropolitana!5e0!3m2!1ses!2scl!4v1494280127569" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>   
<!-- Modal Structure -->
<div id="modal3" class="modal">
	  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3326.59952553488!2d-70.65910894941109!3d-33.51179500795448!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9662dae803c860cd%3A0x5dc2fdd1c6c5e4ea!2sGran+Avenida+Jose+Miguel+Carrera+6150%2C+San+Miguel%2C+Regi%C3%B3n+Metropolitana!5e0!3m2!1ses!2scl!4v1494280211029" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>
<!-- Modal Structure -->
<div id="modal4" class="modal">
	  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3330.8808881865766!2d-70.57758894941456!3d-33.40027160223018!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9662cf281489f3a3%3A0x3dde1d72dfe3c6e0!2sAv.+Pdte.+Kennedy+Lateral+5601%2C+Las+Condes%2C+Regi%C3%B3n+Metropolitana!5e0!3m2!1ses!2scl!4v1494280251710" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>
<!-- Modal Structure -->
<div id="modal5" class="modal">
	  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3325.3791052800043!2d-70.56710564941002!3d-33.543525209586356!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9662d1230bc150cd%3A0xa536fc343dd95197!2sSta+Amalia+1763%2C+La+Florida%2C+Regi%C3%B3n+Metropolitana!5e0!3m2!1ses!2scl!4v1494280371501" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>   
<!-- Modal Structure -->
<div id="modal6" class="modal">
	  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3318.151080332078!2d-70.73812284940387!3d-33.730909219251586!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9663213c899d6b7b%3A0x20dddc77e7317b90!2sSan+Mart%C3%ADn+173%2C+Buin%2C+Regi%C3%B3n+Metropolitana!5e0!3m2!1ses!2scl!4v1494280404162" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>   
<!-- Modal Structure -->
<div id="modal7" class="modal">
	  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3323.231519293403!2d-70.57994654940812!3d-33.59929691245798!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9662d703b05dd855%3A0x3839d12fe9128d33!2sAv.+Concha+Y+Toro+1149%2C+Puente+Alto%2C+Regi%C3%B3n+Metropolitana!5e0!3m2!1ses!2scl!4v1494280448576" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>
<!-- Modal Structure -->
<div id="modal8" class="modal">
	  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3328.8464340210253!2d-70.65482444941281!3d-33.45330720495022!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9662c50e8dc8cc81%3A0xf0a88cf77b450d2d!2sNataniel+Cox+620%2C+Santiago%2C+Regi%C3%B3n+Metropolitana!5e0!3m2!1ses!2scl!4v1494280469334" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>
<!-- Modal Structure -->
<div id="modal9" class="modal">
	  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3327.7955925900715!2d-70.74911334941199!3d-33.48067220635518!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9662c316795cf5a3%3A0xf6757d0bf2a8fb27!2sAv.+Los+Pajaritos+4500%2C+Maip%C3%BA%2C+Regi%C3%B3n+Metropolitana!5e0!3m2!1ses!2scl!4v1494280540242" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>   
<!-- Modal Structure -->
<div id="modal10" class="modal">
	  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3326.3878861254707!2d-70.71962904941077!3d-33.51729940823747!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9662dca7419ab291%3A0xfebfebddff97232a!2sMall+Plaza+Oeste!5e0!3m2!1ses!2scl!4v1494280565208" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>   
<!-- Modal Structure -->
<div id="modal11" class="modal">
	  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3328.9466668456416!2d-70.694925749413!3d-33.45069600481617!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9662c45fd9596edf%3A0xba3a0f4a0984f296!2sAv.+Padre+Alberto+Hurtado+60%2C+Santiago%2C+Estaci%C3%B3n+Central%2C+Regi%C3%B3n+Metropolitana!5e0!3m2!1ses!2scl!4v1494280587065" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>
<!-- Modal Structure -->
<div id="modal12" class="modal">
	  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d26642.3001110419!2d-70.61616488197272!3d-33.41574779250502!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9662cf26243dcc31%3A0xc9aacb64a9fec91!2sParque+Arauco!5e0!3m2!1ses!2scl!4v1494280627276" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>


<div id="top5">
  <div style="padding-top: 160px;">
    <div class="section">
      <!--   Icon Section   -->
      <div class="row">
        <div class="col s0 m1 esconder">
        </div>
        <?php $contador = 1;?>
        <?php foreach($ranking_piola as $row_rank_p):?>
        <div class="col s6 m4 l2">
          <div class="icon-block">
	          <?php
		          if(!empty($row_rank_p['foto'])):
		          	$imagen = 'upload/'.$row_rank_p['foto'];
		          else:
		          	$imagen = 'images/avatar.jpg';
		          endif;
	          ?>
          	<div id="foto" style="background:url(<?=$imagen?>);"><img src="images/marcofoto.png" alt="marcofoto" width="100%" height="" /></div>
          	<div id="lugar"><?=$contador?>º Lugar</div>
            <div id="puntaje"><?=$row_rank_p['puntaje']?> pts</div>
          	<div id="<?=$row_rank_p['pais']?>"></div>
          </div>
        </div>
       <?php $contador++;?>
       <?php endforeach;?>
        <div class="col s0 m1">
        </div>
        
      </div>
      
      
      <center><input type="text" name="search" id="search" placeholder="Encuentra tu ranking" style="width:35%" /></center>
      <ul id="finalResult"></ul>
<br><br>
<div style="text-align: right; margin-right: 30px; font-size: 40px; color: #fff; font-family: 'orbitron',sans-serif; text-shadow:1px 1px 10px #08d9ec"><i><a href="<?=base_url('welcome/ranking')?>">VER MÁS</a></i></div>
    </div>
  </div>
</div>

<!--
<div id="mundial">
  <div class="container">
    <div class="section">
      <!--   Icon Section   
      <div class="row">
	      <br><br><br>
	      <br><br><br>
        <div class="col s6 m2">
          <div class="icon-block">
          	<div id="foto"><img src="images/marcofoto.png" alt="marcofoto" width="100%" height="" /></div>
          	<div id="lugar">1er Lugar</div>
            <div id="puntaje">25.000</div>
          	<div id="chile"></div>
          </div>
        </div>

        <div class="col s6 m2">
          <div class="icon-block">
          	<div id="foto"><img src="images/marcofoto.png" alt="marcofoto" width="100%" height="" /></div>
          	<div id="lugar">2º Lugar</div>
            <div id="puntaje">25.000</div>
          	<div id="colombia"></div>
          </div>
        </div>

        <div class="col s6 m2">
          <div class="icon-block">
          	<div id="foto"><img src="images/marcofoto.png" alt="marcofoto" width="100%" height="" /></div>
          	<div id="lugar">3er Lugar</div>
            <div id="puntaje">25.000</div>
          	<div id="peru"></div>
          </div>
        </div>
        
        <div class="col s6 m2">
          <div class="icon-block">
          	<div id="foto"><img src="images/marcofoto.png" alt="marcofoto" width="100%" height="" /></div>
          	<div id="lugar">4º Lugar</div>
            <div id="puntaje">25.000</div>
          	<div id="costarica"></div>
          </div>
        </div>
        
        <div class="col s6 m2">
          <div class="icon-block">
          	<div id="foto"><img src="images/marcofoto.png" alt="marcofoto" width="100%" height="" /></div>
          	<div id="lugar">5º Lugar</div>
            <div id="puntaje">25.000</div>
          	<div id="argentina"></div>
          </div>
        </div>
        
        <div class="col s6 m2">
          <div class="icon-block">
          	<div id="foto"><img src="images/marcofoto.png" alt="marcofoto" width="100%" height="" /></div>
          	<div id="lugar">6º Lugar</div>
            <div id="puntaje">25.000</div>
          	<div id="argentina"></div>
          </div>
        </div>
      </div>
      
      <div class="row">
        <div class="col s6 m2">
          <div class="icon-block">
          	<div id="foto"><img src="images/marcofoto.png" alt="marcofoto" width="100%" height="" /></div>
          	<div id="lugar">7º Lugar</div>
            <div id="puntaje">25.000</div>
          	<div id="chile"></div>
          </div>
        </div>

        <div class="col s6 m2">
          <div class="icon-block">
          	<div id="foto"><img src="images/marcofoto.png" alt="marcofoto" width="100%" height="" /></div>
          	<div id="lugar">8º Lugar</div>
            <div id="puntaje">25.000</div>
          	<div id="colombia"></div>
          </div>
        </div>

        <div class="col s6 m2">
          <div class="icon-block">
          	<div id="foto"><img src="images/marcofoto.png" alt="marcofoto" width="100%" height="" /></div>
          	<div id="lugar">9º Lugar</div>
            <div id="puntaje">25.000</div>
          	<div id="peru"></div>
          </div>
        </div>
        
        <div class="col s6 m2">
          <div class="icon-block">
          	<div id="foto"><img src="images/marcofoto.png" alt="marcofoto" width="100%" height="" /></div>
          	<div id="lugar">10º Lugar</div>
            <div id="puntaje">25.000</div>
          	<div id="costarica"></div>
          </div>
        </div>
        
        <div class="col s6 m2">
          <div class="icon-block">
          	<div id="foto"><img src="images/marcofoto.png" alt="marcofoto" width="100%" height="" /></div>
          	<div id="lugar">11º Lugar</div>
            <div id="puntaje">25.000</div>
          	<div id="argentina"></div>
          </div>
        </div>
        
        <div class="col s6 m2">
          <div class="icon-block">
          	<div id="foto"><img src="images/marcofoto.png" alt="marcofoto" width="100%" height="" /></div>
          	<div id="lugar">12º Lugar</div>
            <div id="puntaje">25.000</div>
          	<div id="argentina"></div>
          </div>
        </div>
      </div>
      
            <div class="row">
        <div class="col s6 m2">
          <div class="icon-block">
          	<div id="foto"><img src="images/marcofoto.png" alt="marcofoto" width="100%" height="" /></div>
          	<div id="lugar">13º Lugar</div>
            <div id="puntaje">25.000</div>
          	<div id="chile"></div>
          </div>
        </div>

        <div class="col s6 m2">
          <div class="icon-block">
          	<div id="foto"><img src="images/marcofoto.png" alt="marcofoto" width="100%" height="" /></div>
          	<div id="lugar">14º Lugar</div>
            <div id="puntaje">25.000</div>
          	<div id="colombia"></div>
          </div>
        </div>

        <div class="col s6 m2">
          <div class="icon-block">
          	<div id="foto"><img src="images/marcofoto.png" alt="marcofoto" width="100%" height="" /></div>
          	<div id="lugar">15º Lugar</div>
            <div id="puntaje">25.000</div>
          	<div id="peru"></div>
          </div>
        </div>
        
        <div class="col s6 m2">
          <div class="icon-block">
          	<div id="foto"><img src="images/marcofoto.png" alt="marcofoto" width="100%" height="" /></div>
          	<div id="lugar">16º Lugar</div>
            <div id="puntaje">25.000</div>
          	<div id="costarica"></div>
          </div>
        </div>
        
        <div class="col s6 m2">
          <div class="icon-block">
          	<div id="foto"><img src="images/marcofoto.png" alt="marcofoto" width="100%" height="" /></div>
          	<div id="lugar">17º Lugar</div>
            <div id="puntaje">25.000</div>
          	<div id="argentina"></div>
          </div>
        </div>
        
        <div class="col s6 m2">
          <div class="icon-block">
          	<div id="foto"><img src="images/marcofoto.png" alt="marcofoto" width="100%" height="" /></div>
          	<div id="lugar">18º Lugar</div>
            <div id="puntaje">25.000</div>
          	<div id="argentina"></div>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>
-->

	      
	      
	      
	      <div id="premios">
				<div class="row">
			        <div class="col s6 m4 l2">
			          <div class="icon-block">
			          	<div id="foto1"><img src="../images/p1.png" alt="marcofoto" width="100%" height="" /></div>
			          	<div id="lugarp">1er Lugar</div>
			            <div id="texto">Drone<br>DJI Phantom</div>
			          </div>
			        </div>
			
			        <div class="col s6 m4 l2">
			          <div class="icon-block">
			          	<div id="foto1"><img src="../images/p2.png" alt="marcofoto" width="100%" height="" /></div>
			          	<div id="lugarp">2º Lugar</div>
			            <div id="texto">Nintento<br>Switch</div>
			          </div>
			        </div>
			
			        <div class="col s6 m4 l2">
			          <div class="icon-block">
			          	<div id="foto1"><img src="../images/p3.png" alt="marcofoto" width="100%" height="" /></div>
			          	<div id="lugarp">3er Lugar</div>
			            <div id="texto">Audifonos<br>Beats</div>
			          </div>
			        </div>
			        
			        <div class="col s6 m4 l2">
			          <div class="icon-block">
			          	<div id="foto1"><img src="../images/p4.png" alt="marcofoto" width="100%" height="" /></div>
			          	<div id="lugarp">4º Lugar</div>
			            <div id="texto">Parlante<br>Bluetooth</div>
			          </div>
			        </div>
			        
			        <div class="col s6 m4 l2">
			          <div class="icon-block">
			          	<div id="foto1"><img src="../images/p5.png" alt="marcofoto" width="100%" height="" /></div>
			          	<div id="lugarp">5º Lugar</div>
			            <div id="texto">Modulus<br>Nerf Tristrike</div>
			          </div>
			        </div>
			        
			        <div class="col s6 m4 l2">
			          <div class="icon-block">
			          	<div id="foto1"><img src="../images/p6.png" alt="marcofoto" width="100%" height="" /></div>
			          	<div id="lugarp">15 Lugares</div>
			            <div id="texto">Nerf<br>Disruptor</div>
			          </div>
			        </div>
			      </div>
	      </div>
	      
	      
 
	      

