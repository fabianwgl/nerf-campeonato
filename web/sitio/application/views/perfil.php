<body>
  <nav role="navigation">
    <div class="nav-wrapper">
      <a href="#" class="brand-logo center"><img src="../images/logotour.png" alt="logotour" width="100%" height="" /></a>
       <ul id="navegacion" class="left hide-on-med-and-down">
        <li><a href="../#">Home</a></li>
        <li><a href="../#instrucciones">Cómo participar</a></li>
        <li><a href="../#donde-participar">Dónde participar</a></li>
      </ul>
      <ul class="right hide-on-med-and-down" id="navegacion">
        <li><a href="../#top5">Ranking</a></li>
        <li><a href="../#premios">Premios</a></li>
        <li><a href="../bases/Bases-de-Torneo-NERF%20TOURNAMENT-CHILE.docx">Bases legales</a></li>
        <li><a href="#" data-target="login" class="login">Login</a></li>
        <li><a href="http://www.nerf.com">Nerf.com</a></li>
      </ul>
      

      
      <ul id="nav-mobile" class="side-nav"> 
        <li><a href="#">Home</a></li>
        <li><a href="#">Cómo participar</a></li>
        <li><a href="#">Dónde participar</a></li>
        <li><a href="#">Ranking</a></li>
        <li><a href="#">Premios</a></li>
        <li><a href="#">Nerf.com</a></li>
        <li><a href="#">Bases legales</a></li>
        <li><a href="#" data-target="login">Login</a></li>
      </ul>
      
      <a href="#" data-activates="nav-mobile" class="button-collapse"><i class="material-icons">menu</i></a>
    </div>
	  
	
  </nav>
  
      <div id="login" class="modal">
	  <center>
	  		<form method="post" action="welcome/login">
			      <div class="row" style="margin-bottom: 10px">
			        <div class="input-field col s6 offset-s3">
			          <input id="email" name="email" type="email">
			          <label for="email">Email</label>
			        </div>
			      </div>
			       <div class="row" style="margin-bottom: 10px">
			        <div class="input-field col s6 offset-s3">
			          <input id="password" name="password" type="password">
			          <label for="password">Password</label>
			        </div>
			      </div>
			      <input type="submit" hidden="hidden" />
			</form>	  
	  </center>
	</div> 



<div id="fondo" style="margin-top: 85px;">
  <div class="container">
    <div class="section" >

      <!--   Icon Section   -->
      <div class="row">
        <div class="col s1 m2 esconder">
        </div>

        <div class="col s12 m12 l8 metal" >
          <div class="row">    
	        <div class="col s12 m6 offset-m3 l4">
		        <div class="titulo">PERFIL</div>
	          <div class="icon-block">
		      <?php
		          if(!empty($usuario[0]['foto'])):
		          	$imagen = '../upload/'.$usuario[0]['foto'];
		          else:
		          	$imagen = '../images/avatar.jpg';
		          endif;
	          ?>
	          	<div id="foto" style="background:url(<?=$imagen?>);"><img src="../../images/marcofoto.png" alt="marcofoto" width="100%" height="" /></div>
	          	<div id="<?=$usuario[0]['pais']?>1"></div>
	          </div>
	        </div>  
	        <div class="col s12 m10 offset-m1 l8">
		        <div class="datos">
					<div class="row" style="margin: 0;padding: 5px;">    
							<div class="col s6 m6">
								<center>Puntaje Máximo<br><span><?=$usuario[0]['puntaje']?> pts</span></center>
							</div>   
							<div class="col s6 m6">
								<center>Ranking<br><span><?=$posicion[0]['rank']?>º Lugar</span></center>
							</div>    
					</div> 	
		        </div>
		        <div class="titulo1"><?=$usuario[0]['nombre']?> <?=$usuario[0]['apellido']?></div>
			      <div class="info">
			        <b>Edad: </b><?=$usuario[0]['edad']?><br>
			        <b>Mail: </b><?=$usuario[0]['email']?><br>
			        <b>Teléfono: </b><?=$usuario[0]['telefono']?></div>     
			      
			      
			      <br>
			      
			      <img src="../../images/mensajes.png" alt="mensajes" width="40%" height="" style="z-index: 2;
    position: relative;" />
<form method="post" action="agregarMensaje" enctype="multipart/form-data">			      
  <select name="mensaje" id="mensaje" class="browser-default test">
    <option value="" disabled selected class="prueba">selecciona tu mensaje</option>
    <option value="" disabled>Desafío</option>    
    <option value="¿Quién será el siguiente que perderá conmigo?">¿Quién será el siguiente que perderá conmigo?</option>
    <option value="Menos blá blá y más lanzamientos ¿quién se anota?">Menos blá blá y más lanzamientos ¿quién se anota?</option>
    <option value="¿Quién será el siguiente que perderá conmigo?">¿Quién será el siguiente que perderá conmigo?</option>
    <option value="Los espero en el torneo aprendices.">Gánenme si pueden ¡Este torneo ya tiene dueño! </option>
    <option value="¿Quién será el siguiente que perderá conmigo?">¿Quién será el siguiente que perderá conmigo?</option>
    <option value="Pasen a hablar con el próximo campeón Nerf de Latinoamérica">¡Devuélvanse a sus casas! Este torneo está a mi nivel PRO</option>
    <option value="" disabled>Autoreferente</option>    
    <option value="Les presento al mejor Nerf Gamer de América: YO">Les presento al mejor Nerf Gamer de América: YO</option>
    <option value="No me verán ni la sombra ¡comiencen a practicar!">No me verán ni la sombra ¡comiencen a practicar!</option>
    <option value="Todos mis dardos van directo al objetivo ¿y los tuyos?">Todos mis dardos van directo al objetivo ¿y los tuyos?</option>
    <option value="Este es mi momento de vencer, nadie me quitará la gloria ">Este es mi momento de vencer, nadie me quitará la gloria </option>
    <option value="No conozco la palabra derrota ¿ustedes si verdad?">No conozco la palabra derrota ¿ustedes si verdad?</option>
    <option value="Cuando compito lo hago para ganar ¡así es como juego!">Cuando compito lo hago para ganar ¡así es como juego!</option>
    <option value="Conmigo aquí el 1er lugar del ranking está lleno ¡Deal with it!">Conmigo aquí el 1er lugar del ranking está lleno ¡Deal with it!</option>
    <option value="" disabled>Consejos</option>    
    <option value="¡Mejor practiquen sus lanzamientos! Conmigo la competencia esta difícil">¡Mejor practiquen sus lanzamientos! Conmigo la competencia esta difícil</option>
    <option value="¿Lanzador? ¡check! ¿dardos? ¡check! ¿GAMER ON FIRE? ¡double check!">¿Lanzador? ¡check! ¿dardos? ¡check! ¿GAMER ON FIRE? ¡double check!</option>
    <option value="Todos mis dardos van directo al objetivo ¿y los tuyos?">Todos mis dardos van directo al objetivo ¿y los tuyos?</option>
    <option value="Siempre en movimiento y con la mira la victoria ¡así es como jugamos!">Siempre en movimiento y con la mira la victoria ¡así es como jugamos!</option>
  </select>
  <input name="id" value="<?=$usuario[0]['id']?>" hidden="hidden"/>

            <div id="mensajes">
	            <?php if(!empty($mensajes)){?>
	            <?php $contador = 1; 
		            foreach($mensajes as $row_mensajes):?>
		    <?php
		          if(!empty($row_mensajes['foto'])):
		          	$foto = '../upload/'.$row_mensajes['foto'];
		          else:
		          	$foto = '../images/avatar.jpg';
		          endif;
	          ?>
		            <?php if(($contador % 2) == 0):?>
		            
	            <div id="fila" class="row">
		            <div class="col s4 m3">
			          	<div id="foto" style="background:url(<?=$foto?>);"><img src="../../images/marcofoto.png" alt="marcofoto" width="100%" height="" /></div>
			          	<div id="lugarr">1er Lugar</div>
			        </div>
		            <div class="col s8 m9">
			            <?=$row_mensajes['mensaje']?>
			            
		            </div>
	            </div>
	            <?php else:?>
	            <div id="fila" class="row">
		            <div class="col s8 m9">
			            <?=$row_mensajes['mensaje']?>
		            </div>
		            <div class="col s4 m3">
			          	<div id="foto" style="background:url(<?=$foto?>);"><img src="../../images/marcofoto.png" alt="marcofoto" width="100%" height="" /></div>
			          	<div id="lugarr">1er Lugar</div>
			        </div>
	            </div>
	            <?php endif;?>
	            <?php $contador++;?>
	            <?php endforeach;?>
	            <?php }?>
            </div>
				  </div>
	<div class="linea row">

<input type="file" name="userfile" size="2048" class="archivo"/>



	<div class="boton">
		<input type="submit" value="Guardar Avatar" />
	</div>

	<div class="boton1">
		<button type="submit" name="mensaje" value="mensaje_nuevo" style="background: transparent;border: 0;">Enviar</button>
	</div>
</form>
	        </div>
	        

	        </div>
          </div>
        </div>

        <div class="col s2 m2">
        </div>
        
      </div>

    </div>
  </div>
</div>