<body>
  <nav role="navigation">
    <div class="nav-wrapper">
      <a href="#" class="brand-logo center"><img src="../images/logotour.png" alt="logotour" width="100%" height="" /></a>
       <ul id="navegacion" class="left hide-on-med-and-down">
        <li><a href="../#">Home</a></li>
        <li><a href="../#instrucciones">Cómo participar</a></li>
        <li><a href="../#donde-participar">Dónde participar</a></li>
      </ul>
      <ul class="right hide-on-med-and-down" id="navegacion">
        <li><a href="../#top5">Ranking</a></li>
        <li><a href="../#premios">Premios</a></li>
        <li><a href="../bases/Bases-de-Torneo-NERF%20TOURNAMENT-CHILE.docx">Bases legales</a></li>
        <li><a href="#" data-target="login" class="login">Login</a></li>
        <li><a href="http://www.nerf.com">Nerf.com</a></li>
      </ul>
      

      
      <ul id="nav-mobile" class="side-nav"> 
        <li><a href="#">Home</a></li>
        <li><a href="#">Cómo participar</a></li>
        <li><a href="#">Dónde participar</a></li>
        <li><a href="#">Ranking</a></li>
        <li><a href="#">Premios</a></li>
        <li><a href="#">Nerf.com</a></li>
        <li><a href="#">Bases legales</a></li>
        <li><a href="#" data-target="login">Login</a></li>
      </ul>
      
      <a href="#" data-activates="nav-mobile" class="button-collapse"><i class="material-icons">menu</i></a>
    </div>
	  
	
  </nav>
  
      <div id="login" class="modal">
	  <center>
	  		<form method="post" action="welcome/login">
			      <div class="row" style="margin-bottom: 10px">
			        <div class="input-field col s6 offset-s3">
			          <input id="email" name="email" type="email">
			          <label for="email">Email</label>
			        </div>
			      </div>
			       <div class="row" style="margin-bottom: 10px">
			        <div class="input-field col s6 offset-s3">
			          <input id="password" name="password" type="password">
			          <label for="password">Password</label>
			        </div>
			      </div>
			      <input type="submit" hidden="hidden" />
			</form>	  
	  </center>
	</div> 

<div id="mundial">
  <div class="">
    <div class="section"> 
      <div class="row">
	      <br><br><br>
	      <center><input type="text" name="search" placeholder="Buscar" style="width: 25%;
    float: right;
    margin: 10px;"></center>
	      <br><br><br>
	      <br><br><br>
	      <?php $contador = 1; 
		      foreach($ranking as $row_ranking):?>
		      
		      <?php
			      if(!empty($row_ranking['foto'])):
			      	$imagen = '../upload/'.$row_ranking['foto'];
			      else:
			      	$imagen = '../images/avatar.jpg';
			      endif; 
		      ?>
        <div class="col s6 m4 l2">
          <div class="icon-block">
          	<div id="foto" style="background:url(<?=$imagen?>);"><img src="../images/marcofoto.png" alt="marcofoto" width="100%" height="" /></div>
          	<div id="lugar"><?=$contador?>° Lugar</div>
            <div id="puntaje"><?=$row_ranking['puntaje']?> pts</div>
          	<div id="<?=$row_ranking['pais']?>"></div>
          </div>
        </div>
        <?php $contador++; 
	        endforeach;?>

<div style="text-align: right; margin-right: 30px; font-size: 40px; color: #fff; font-family: 'orbitron',sans-serif; text-shadow:1px 1px 10px #08d9ec"><i>Siguiente</i></div>

    </div>
  </div>
</div>
