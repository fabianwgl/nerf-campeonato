<body>
  <nav role="navigation" style="position: relative;">
    <div class="nav-wrapper container">
      <a id="logo-container" href="#" class="brand-logo center"><img src="../images/logonerf.png" alt="logonerf" width="100%"></a>
      <!--<ul class="right hide-on-med-and-down">
        <li><a href="#">Navbar Link</a></li>
      </ul>

      <ul id="nav-mobile" class="side-nav">
        <li><a href="#">Navbar Link</a></li>
      </ul>
      <a href="#" data-activates="nav-mobile" class="button-collapse"><i class="material-icons">menu</i></a>-->
    </div>
  </nav>


<div id="fondo" style="margin-top: -80px;">
  <div class="container">
    <div class="section" >

      <!--   Icon Section   -->
      <div class="row">
        <div class="col s2 m2">
        </div>

        <div class="col s8 m8 metal" >
          <div class="row">    
	        <div class="col s4 m4">
		        <br><br>
		        <img src="../images/logotour.png" alt="logotour" width="100%" height="" />
	        </div>  
	        <div class="col s8 m8">
		        <div class="titulo">INGRESAR PUNTAJE</div>
		        <form class="col s12" method="post" action="resultadoBusqueda">
			      <div class="row" style="margin-bottom: 10px">
			        <div class="input-field col s12">
			          <input id="nombre" name="nombre" type="text" >
			          <label for="nombre">Nombre</label>
			        </div>
			      </div>
			      <br><br>
			        <button class="btn waves-effect waves-light" type="submit" name="action" style="line-height: normal;
    background: url(../images/fondopuntos.png);
    background-size: cover;
    border-radius: 10px !important;
    color: #fff;
    font-weight: lighter;
    font-family: 'orbitron',sans-serif;">Buscar
					</button>
			    </form>
			    <?php if(!empty($resultado)){?>
			    <table>
			    	<thead>
			    		<th>Nombre</th>
			    		<th>Apellido</th>
			    		<th>Pais</th>
			    		<th>Intentos</th>
			    		<th>Puntaje</th>
			    	</thead>
			    	<tbody>
			    		<?php foreach($resultado as $row_resultados):?>
			    		<tr>
			    			<td><?=$row_resultados['nombre']?></td>
			    			<td><?=$row_resultados['apellido']?></td>
			    			<td><?=$row_resultados['pais']?></td>
			    			<td><?=$row_resultados['intento']?></td>
			    			<form method="post" action="ingresoPuntaje">
			    			<td><input type="text" name="puntaje" id="puntaje"/></td>
			    			<td><input type="text" name="intento" id="intento" hidden value="<?=$row_resultados['intento']?>"/></td>
			    			<td><input type="text" name="id" id="id" hidden value="<?=$row_resultados['id']?>" /></td>
			    			<td><input type="text" name="actual" id="actual" hidden value="<?=$row_resultados['puntaje']?>" /></td>
			    			<td><button type="submit" name="enviar">Ingresar</button></td>
			    			</form>
			    		</tr>
			    		<?php endforeach;?>	
			    	</tbody>
			    </table>
			    <?php } ?>
	        </div>
          </div>
        </div>

        <div class="col s2 m2">
        </div>
        
      </div>

    </div>
  </div>
</div>