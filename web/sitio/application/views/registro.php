<body>
  <nav role="navigation" style="position: relative;">
    <div class="nav-wrapper container">
      <a id="logo-container" href="#" class="brand-logo center"><img src="../images/logonerf.png" alt="logonerf" width="100%"></a>
      <!--<ul class="right hide-on-med-and-down">
        <li><a href="#">Navbar Link</a></li>
      </ul>

      <ul id="nav-mobile" class="side-nav">
        <li><a href="#">Navbar Link</a></li>
      </ul>
      <a href="#" data-activates="nav-mobile" class="button-collapse"><i class="material-icons">menu</i></a>-->
    </div>
  </nav>

<?php if(!empty($exito)){?>
<h3>REGISTRO EXITOSO</h3>
<?php }?>


<div id="fondo" style="margin-top: -80px;">
  <div class="container">
    <div class="section" >

      <!--   Icon Section   -->
      <div class="row">
        <div class="col s2 m2">
        </div>

        <div class="col s8 m8 metal" >
          <div class="row">    
	        <div class="col s4 m4">
		        <br><br>
		        <img src="../images/logotour.png" alt="logotour" width="100%" height="" />
	        </div>  
	        <div class="col s8 m8">
		        <div class="titulo">REGISTRO</div>
		        <form class="col s12" method="post" action="insertRegistro">
			      <div class="row" style="margin-bottom: 10px">
			        <div class="input-field col s12">
			          <input id="nombre" name="nombre" type="text" >
			          <label for="nombre">Nombre</label>
			        </div>
			      </div>
			      <div class="row" style="margin-bottom: 10px">
			        <div class="input-field col s12">
			          <input id="apellido" name="apellido" type="text" >
			          <label for="apellido">Apellido</label>
			        </div>
			      </div>
			      <div class="row" style="margin-bottom: 10px">
			        <div class="input-field col s12">
			          <input id="edad" name="edad" type="text" >
			          <label for="edad">Edad</label>
			        </div>
			      </div>
			      <div class="row" style="margin-bottom: 10px">
			        <div class="input-field col s12">
			          <input id="telefono" name="telefono" type="number">
			          <label for="telefono">Telefono</label>
			        </div>
			      </div>
			      <div class="row" style="margin-bottom: 0px">
			        <div class="input-field col s12">
						  <select id="pais" name="pais" class="browser-default" style="background: url('../images/fondopuntos.png');background-size: cover;border: 0;color: #fff;font-weight: lighter;font-family: 'orbitron',sans-serif;">
						    <option value="" disabled="" selected="">País</option>
						    <option value="argentina">Argentina</option>
						    <option value="chile">Chile</option>
						    <option value="colombia">Colombia</option>
						    <option value="costarica">Costa Rica</option>
						    <option value="peru">Perú</option>
						</select>
			        </div>
			      </div>
			      <div class="row" style="margin-bottom: 10px">
			        <div class="input-field col s12">
			          <input id="email" name="email" type="email">
			          <label for="email">Email</label>
			        </div>
			      </div>
			       <div class="row" style="margin-bottom: 10px">
			        <div class="input-field col s12">
			          <input id="password" name="password" type="password">
			          <label for="password">Password</label>
			        </div>
			      </div>
			      <div class="row" style="margin-bottom: 10px">
			        <div class="input-field col s12">
			          <input id="puntaje" name="puntaje" type="text" >
			          <label for="puntaje">Puntaje</label>
			        </div>
			      </div>
			      <br><br>
			        <button class="btn waves-effect waves-light" type="submit" name="action" style="line-height: normal;
    background: url(../images/fondopuntos.png);
    background-size: cover;
    border-radius: 10px !important;
    color: #fff;
    font-weight: lighter;
    font-family: 'orbitron',sans-serif;">Registrarse
					</button>
			    </form>
	        </div>
          </div>
        </div>

        <div class="col s2 m2">
        </div>
        
      </div>

    </div>
  </div>
</div>