 <footer class="page-footer teal" style="z-index: 9;">
    <div class="container">
      <div class="row">
        <div class="col l3 s6">
          <center><img src="../images/logonerf.png" alt="logonerf" width="90%" style="padding: 40px 0;"></center>


        </div>
        <div class="col l2 s6">
          <ul class="menufooter">
        <li><a href="#">Home</a></li>
        <li><a href="#">Dónde participar</a></li>
        <li><a href="#">Cómo participar</a></li>
        <li><a href="#">Ranking</a></li>
        <li><a href="#">Premios</a></li>
        <li><a href="#">Nerf.com</a></li>
        <li><a href="#">Bases legales</a></li>
        <li><a href="#">Login</a></li>
          </ul>
        </div>
        <div class="col l7 s12">
          <img src="../images/footer.png" alt="footer" width="100%" height="" />
        </div>
      </div>
    </div>
    <div class="footer-copyright">
      <div class="container">
      Desarrollado por <a class="brown-text text-lighten-3" href="http://chucara.cl">Chúcara</a>
      </div>
    </div>
  </footer>


  <!--  Scripts-->
  <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
  <script src="../js/materialize.js"></script>
  <script src="../js/init.js"></script>
<script>
    $( document ).ready(function() {
      $('.modal').modal();
      $('#modal1').on('click', function() {
      });
    });
  </script>
  </body>
</html>
