<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->model('principal_model');
		$ranking_total = $this->principal_model->rankingTotal();
		$ranking_piola = $this->principal_model->rankingPiola();
		$mensajes = $this->principal_model->mostrarMensajesInicio();
		$data = array('ranking_t' => $ranking_total, 'ranking_piola' => $ranking_piola, 'mensajes' => $mensajes);
		$this->load->view('header_index');
		$this->load->view('index', $data);
		$this->load->view('footer_index');
	}
	
	public function buscarUsuario(){
		$this->load->model('principal_model');
		$buscar = $this->input->post('search');
		print_r($buscar);
		$query = $this->principal_model->buscarJugador($buscar);
		echo json_encode ($query);
		
	}


	public function login(){
		$this->load->model('principal_model');
		$usuario = $this->input->post('email');
		$password = $this->input->post('password');
		$usuario = $this->principal_model->inicioSesion($usuario, $password);
		if($usuario):
			$mensajes = $this->principal_model->mostrarMensajesUsuario($usuario[0]['id']);
			$posicion = $this->principal_model->posicion($usuario[0]['id']);
			$data = array('usuario' => $usuario, 'mensajes' => $mensajes, 'posicion' => $posicion);
			$this->load->view('header');
			$this->load->view('perfil', $data);
			$this->load->view('footer');
		else:
			redirect('welcome/index', 'refresh');
		endif;
	}
	
	public function agregarMensaje(){
		$this->load->model('principal_model');
		$this->load->helper(array('form', 'url'));
		$mensaje = $this->input->post('mensaje');
		$id = $this->input->post('id');
		if($mensaje){
			$id = $this->input->post('id');
			$insert = $this->principal_model->insertarMensaje($id, $mensaje);
			if($insert):
				redirect('welcome/index', 'refresh');
			else:
				print_r('mensaje no insertado');
				die();
			endif;
		}else{
			    $config['upload_path']          = './upload/';
                $config['allowed_types']        = 'gif|jpg|png';
                $config['max_size']             = 2048;
                $config['max_width']            = 1024;
                $config['max_height']           = 768;
                $this->upload->initialize($config);
                $this->load->library('upload', $config);
                $userfile = 'userfile';
                if ( ! $this->upload->do_upload($userfile))
                {
                        $error = array('error' => $this->upload->display_errors());
                        $this->load->view('upload_form', $error);
                }
                else
                {
                        $data = array('upload_data' => $this->upload->data());
                        $userfiles = $this->upload->data('file_name');
                        $imagen = $this->principal_model->imagen($userfiles, $id);
                        redirect('welcome', 'refresh');
                }
		}
	}

	public function registro(){
		$this->load->view('header');
		$this->load->view('registro');
		$this->load->view('footer');
	}

	public function insertRegistro(){
		$this->load->model('principal_model');
		$this->load->library('session');
		$nombre = $this->input->post('nombre');
		$apellido = $this->input->post('apellido');
		$telefono = $this->input->post('telefono');
		$pais = $this->input->post('pais');
		$email = $this->input->post('email');
		$password = $this->input->post('password');
		$edad = $this->input->post('edad');
		$puntaje = $this->input->post('puntaje');
		$registro = $this->principal_model->insertarRegistro($nombre,$apellido,$telefono,$pais,$email,$password,$edad,$puntaje);
		if($registro):
			$this->session->set_flashdata('exito', 'mensaje exitoso');
			redirect('welcome/registro','refresh');
		else:
			redirect('welcome/registro');
		endif;
	}
	
	
	public function ranking(){
		$this->load->model('principal_model');
		$ranking = $this->principal_model->rankingTotal();
		$data = array('ranking' => $ranking);
		$this->load->view('header');
		$this->load->view('ranking', $data);
		$this->load->view('footer');
	}

	public function ingresarPuntaje(){
		$this->load->view('header');
		$this->load->view('ingreso_puntaje');
		$this->load->view('footer');
	}

	public function resultadoBusqueda(){
		$this->load->model('principal_model');
		$nombre = $this->input->post('nombre');
		$resultado = $this->principal_model->busqueda($nombre);
		if(!empty($resultado)):
			$data = array('resultado' => $resultado);
			$this->load->view('header');
			$this->load->view('ingreso_puntaje', $data);
			$this->load->view('footer');
		else:
			redirect('welcome/ingresar_puntaje', 'refresh');
		endif;	
	}

	public function ingresoPuntaje(){
		$this->load->model('principal_model');
		$id = $this->input->post('id');
		$puntaje = $this->input->post('puntaje');
		$intento = $this->input->post('intento');
		$actual = $this->input->post('actual');
		$ingreso = $this->principal_model->sumarPuntaje($id,$puntaje,$intento,$actual);
		if($ingreso):
			redirect('welcome/ingresarPuntaje', 'refresh');
		else:
			redirect('welcome/ingresarPuntaje', 'refresh');
		endif;
	}
}
