-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 11-05-2017 a las 00:54:16
-- Versión del servidor: 5.7.14
-- Versión de PHP: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `nerf`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `admin`
--

CREATE TABLE `admin` (
  `id_admin` int(255) NOT NULL,
  `mail` varchar(255) NOT NULL,
  `pass` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `admin`
--

INSERT INTO `admin` (`id_admin`, `mail`, `pass`) VALUES
(1, 'admin@admin.cl', 'admin');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mensajes`
--

CREATE TABLE `mensajes` (
  `id` int(11) NOT NULL,
  `usuario_id` int(11) NOT NULL,
  `mensaje` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fecha` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `mensajes`
--

INSERT INTO `mensajes` (`id`, `usuario_id`, `mensaje`, `fecha`) VALUES
(3, 1, '¿Quién será el siguiente que perderá conmigo?', '0000-00-00 00:00:00'),
(2, 1, '¿Quién será el siguiente que perderá conmigo?', '0000-00-00 00:00:00'),
(4, 2, 'Los espero en el torneo aprendices.', '0000-00-00 00:00:00'),
(5, 4, '¿Quién será el siguiente que perderá conmigo?', '0000-00-00 00:00:00'),
(6, 2, 'Los espero en el torneo aprendices.', '0000-00-00 00:00:00'),
(7, 2, '¿Quién será el siguiente que perderá conmigo?', '0000-00-00 00:00:00'),
(8, 2, 'Pasen a hablar con el próximo campeón Nerf de Latinoamérica', '0000-00-00 00:00:00'),
(9, 4, 'Menos blá blá y más lanzamientos ¿quién se anota?', '0000-00-00 00:00:00'),
(10, 2, 'Conmigo aquí el 1er lugar del ranking está lleno ¡Deal with it!', '0000-00-00 00:00:00'),
(11, 4, 'Menos blá blá y más lanzamientos ¿quién se anota?', '0000-00-00 00:00:00'),
(12, 2, '¡Mejor practiquen sus lanzamientos! Conmigo la competencia esta difícil', '0000-00-00 00:00:00'),
(17, 2, 'mensaje_nuevo', '0000-00-00 00:00:00'),
(16, 2, 'mensaje_nuevo', '0000-00-00 00:00:00'),
(18, 2, 'mensaje_nuevo', '0000-00-00 00:00:00'),
(19, 2, 'mensaje_nuevo', '0000-00-00 00:00:00'),
(20, 2, 'mensaje_nuevo', '0000-00-00 00:00:00'),
(21, 2, 'mensaje_nuevo', '0000-00-00 00:00:00'),
(22, 2, 'mensaje_nuevo', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `apellido` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `edad` int(11) NOT NULL,
  `telefono` int(11) NOT NULL,
  `pais` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `intento` int(11) NOT NULL,
  `puntaje` int(11) NOT NULL,
  `ranking` int(11) NOT NULL,
  `foto` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `nombre`, `apellido`, `edad`, `telefono`, `pais`, `email`, `password`, `intento`, `puntaje`, `ranking`, `foto`) VALUES
(1, 'esteban', 'vivanco', 25, 90885098, 'chile', 'evivanco.10@gmail.com', '1darkest1', 1, 50, 0, 'avatar.jpg'),
(5, 'Test', 'Test', 12, 12312312, 'costarica', 'rodrigo@chucara.cl', '12345', 1, 12345, 0, 'mati.jpg'),
(6, 'prueba', 'prueba', 123, 123123, 'peru', 'test@test.cl', 'asdasdasd', 1, 5400, 0, 'avatar.jpg'),
(7, 'test', 'test', 0, 456456, 'colombia', 'asdad@fghgj.ghj', 'asdasdasd', 1, 123132, 0, '1303695661221_f.jpg'),
(8, 'mitest', 'juan', 22, 12342342, 'chile', 'r@r.cl', '', 1, 1231, 0, 'avatar.jpg'),
(9, 'f', 'f', 22, 2, 'chile', 'r.aaa@c.cl', 'asd', 1, 290, 0, 'avatar.jpg');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indices de la tabla `mensajes`
--
ALTER TABLE `mensajes`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `admin`
--
ALTER TABLE `admin`
  MODIFY `id_admin` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `mensajes`
--
ALTER TABLE `mensajes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
